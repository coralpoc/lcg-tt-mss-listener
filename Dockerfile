# Base runtime image
FROM mcr.microsoft.com/dotnet/aspnet:8.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 8080
#EXPOSE 443

#ENV Variables
ENV ASPNETCORE_URLS=http://+:80;http://+:8080
#ENV ASPNETCORE_HTTP_PORTS=80;8080;443

FROM mcr.microsoft.com/dotnet/sdk:8.0-alpine AS build
ENV http_proxy="http://infrastructure-proxy-ie2.linux.corp:3128" \
    https_proxy="http://infrastructure-proxy-ie2.linux.corp:3128" \
    no_proxy="lcgdevops-docker-local.dev.docker.env.works,artifactory.bwinparty.corp"

WORKDIR /src
COPY *.sln ./
COPY nuget.config ./

RUN apk update && apk add --no-cache ca-certificates

COPY ./certs/* /usr/local/share/ca-certificates/
RUN update-ca-certificates && ls -alh /usr/local/share/ca-certificates/*

#COPY ["Specials.Listener.Service/Specials.Listener.Service.csproj", "Specials.Listener.Service/"]
COPY tests/Specials.Listener.Service.Tests/Specials.Listener.Service.Tests.csproj /src/tests/Specials.Listener.Service.Tests/Specials.Listener.Service.Tests.csproj
COPY Specials.Listener.Service/Specials.Listener.Service.csproj /src/Specials.Listener.Service/Specials.Listener.Service.csproj

#.net diagnostic tool installations
RUN dotnet tool install dotnet-dump --tool-path /tools && \
    dotnet tool install dotnet-counters --tool-path /tools && \
    dotnet tool install dotnet-trace --tool-path /tools && \
    dotnet tool install dotnet-gcdump --tool-path /tools && \
    dotnet restore "Specials.Listener.Service\Specials.Listener.Service.csproj"

# Copy everything else and build
COPY ./ ./

# Run Test cases
RUN dotnet test "tests/Specials.Listener.Service.Tests/Specials.Listener.Service.Tests.csproj" -c Release

# Build test image
FROM build AS test

# Build publish image
FROM build AS publish
ARG VERSION
RUN dotnet publish "Specials.Listener.Service/Specials.Listener.Service.csproj" -c "Release" -o /app/publish 

#Final Runtime Image
FROM base AS final
LABEL maintainer="lcg-trading-technology"

WORKDIR /app
COPY --from=publish /app/publish .
COPY --from=build /tools /tools
ENV PATH="/tools:${PATH}"

# Copy AppDynamics Config files
RUN mkdir -p /opt/appdynamics/dotnet && chmod 755 /opt/appdynamics/dotnet
COPY new-files/libappdprofiler.so /opt/appdynamics/dotnet/
COPY new-files/AppDynamics.Agent.netstandard.dll /opt/appdynamics/dotnet/
COPY new-files/AppDynamicsConfig.json /opt/appdynamics/dotnet/
COPY new-files/libappdprofiler_musl.so /opt/appdynamics/dotnet/
COPY new-files/libappdprofiler_glibc.so /opt/appdynamics/dotnet/
COPY new-files/AppDynamics.Agent.netstandard.dll /opt/appdynamics/dotnet/
COPY new-files/AppDynamics.Agent.OtelSDK.dll /opt/appdynamics/dotnet/
COPY new-files/AppDynamics.HeapDump.Library.dll /opt/appdynamics/dotnet/
COPY certs/appdCert.cer /opt/appdynamics/dotnet/conf/
COPY certs/appdCert-nonprod.cer /opt/appdynamics/dotnet/conf/

RUN ls /opt/appdynamics/dotnet/conf

# Set environment variables
ARG APP_SHA1
ENV APP_SHA1=$APP_SHA1

# Mandatory settings required to attach the agent to the .NET application & Configure connection to the controller
ENV CORECLR_PROFILER={57e1aa68-2229-41aa-9931-a6e93bbc64d8} \
    CORECLR_ENABLE_PROFILING=1 \
    CORECLR_PROFILER_PATH=/opt/appdynamics/dotnet/libappdprofiler.so \
    APPDYNAMICS_CONTROLLER_PORT=443 \
    APPDYNAMICS_AGENT_ACCOUNT_NAME="customer1"

# Start the application
ENTRYPOINT ["dotnet", "Specials.Listener.Service.dll"]
