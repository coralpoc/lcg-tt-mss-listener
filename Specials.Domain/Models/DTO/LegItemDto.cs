﻿using System;
using System.Collections.Generic;

namespace Specials.Domain.Models.DTO
{
    public class LegItemDto
    {
        public long Id { get; set; }
        public string KeyId => $"leg:ch:{Channel}:e:{EventId}:m:{MarketId}:s:{Id}";
        public string Type => "leg";
        public string Channel { get; set; }


        public double Price { get; set; }
        public double RoundedPrice => Math.Round(Price, 2);
        public string FractionalPrice { get; set; }

        public string DisplayName { get; set; }

        public string SelectionStatus { get; set; }
        public string SelectionDisplayStatus { get; set; }

        public long EventId { get; set; }
        public string EventName { get; set; }
        public bool EventIsFinished { get; set; }
        public bool EventIsLive { get; set; }
        public bool EventIsResulted { get; set; }
        public bool EventIsStarted { get; set; }
        public string EventStatus { get; set; }
        public string EventDisplayStatus { get; set; }
        public string EventDate { get; set; }

        public long MarketId { get; set; }
        public string MarketStatus { get; set; }
        public string MarketDisplayStatus { get; set; }
        public string MarketName { get; set; }

        public string ResultCode { get; set; }
        public bool ResultConfirmed { get; set; }
        public bool IsSettled { get; set; }

        public List<string> SpecialKeyIds { get; set; }
        public string SummaryStatus { get; set; }
    }
}