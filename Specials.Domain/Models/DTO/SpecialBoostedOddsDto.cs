﻿namespace Specials.Domain.Models.DTO
{
    public class SpecialBoostedOddsDto
    {
        public long SpecialId { get; set; }
        public string SpecialKeyId { get; set; }
        public int BoostIndex { get; set; }
        public double AccumulativeOdds { get; set; }
        public string FractionalPrice { get; set; }
        public string ToPrice { get; set; }
        public string DisplayName { get; set; }
    }
}