﻿using System;
using System.Collections.Generic;

namespace Specials.Domain.Models.DTO
{
    public class SpecialDto 
    {
        public long Id { get; set; }
        public string KeyId => $"{Type}:ch:{Channel}:e:{EventId}:m:{MarketId}:s:{Id}";
        public string Type => "special";
        public string Channel { get; set; }


        public double Price { get; set; }
        public double RoundedPrice => Math.Round(Price, 2);
        public string FractionalPrice { get; set; }
        public string BoostFractionalPrice { get; set; }
        public int BoostDecimalPrice { get; set; }
        public int BoostIndex { get; set; }

        public string DisplayName { get; set; }

        public long EventId { get; set; }
        public string EventName { get; set; }
        public string EventDate { get; set; }
        public bool EventIsStarted { get; set; }
        public string EventStatus { get; set; }
        public string EventDisplayStatus { get; set; }

        public long MarketId { get; set; }
        public string MarketName { get; set; }
        public string MarketStatus { get; set; }
        public string MarketDisplayStatus { get; set; }

        public string SelectionStatus { get; set; }
        public string SelectionDisplayStatus { get; set; }

        public bool SendUpdates { get; set; }
        public bool PauseUpdate { get; set; }

        public List<long> LegItemIds { get; set; }
        public List<string> LegItemKeys { get; set; }
        public List<long> LegEventIds { get; set; }

        public string SummaryStatus { get; set; }
        public bool IsWasPricePresent { get; set; }
    }
}