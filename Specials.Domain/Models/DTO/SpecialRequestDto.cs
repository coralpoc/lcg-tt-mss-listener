﻿using System.Collections.Generic;

namespace Specials.Domain.Models.DTO
{
    public class SpecialRequestDto
    {
        public long Id { get; set; }
        public string Channel { get; set; }
        public List<long> Selections { get; set; }
    }
}