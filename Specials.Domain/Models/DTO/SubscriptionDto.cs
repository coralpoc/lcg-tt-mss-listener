﻿using System;
using System.Collections.Generic;

namespace Specials.Domain.Models.DTO
{
    public class SubscriptionDto
    {
        public string Channel { get; set; }
        public string KeyId => $"subs:ch:{Channel}:e:{EventId}";
        public long EventId { get; set; }
        public List<string> SpecialKeyIds { get; set; }
        public string PodName { get; set; }
        public DateTime Subscribed { get; set; }

        public override string ToString()
        {
            return
                $"\n Subscribed: {nameof(KeyId)}:{KeyId} {nameof(PodName)}:{PodName}  {nameof(Subscribed)}:{Subscribed}";
        }
    }
}