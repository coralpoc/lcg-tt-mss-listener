﻿using System.Collections.Generic;

namespace Specials.Domain.Models.Domain
{
    public class Config : Dictionary<string, ConfigDetail>
    {
        public Config()
        {
            this["ld"] = new ConfigDetail();
            this["cd"] = new ConfigDetail();
        }

        public string ContainerImagePath { get; set; }
        public string RedisCluster { get; set; }
        public string RedisExpiry { get; set; }
        public string RedisDbName { get; set; }
        public string ContainerIp { get; set; }
        public string HostName { get; set; }
        public string ContainerName { get; set; }
        public string PrivateIp { get; set; }
        public string MinimumLoggingLevel { get; set; }
        public string Environment { get; set; }
        public string BaseUrl { get; set; }

        public string KafkaServer { get; set; }
        public string KafkaTopic { get; set; }

        public string SubscriptionCleanupTime { get; set; }
        public string SubscriptionRefreshTime { get; set; }
        public string App { get; set; }
        public string LogFilePath { get; set; }
        public long LogFileSizeLimit { get; set; }

        public string KeepAlive { get; set; }
        public string ConnectTimeout { get; set; }
        public string SyncTimeout { get; set; }

        public double listenerhttpclientConnectionTimeout { get; set; }
        public int listenerhttpclientRetrycount { get; set; }
        public double listenerhttpclientRetrydurtaion { get; set; }
    }
}