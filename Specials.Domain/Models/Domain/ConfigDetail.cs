﻿namespace Specials.Domain.Models.Domain
{
    public class ConfigDetail
    {
        public string OxiUrl { get; set; }
        public string OxiUser { get; set; }
        public string OxiPassword { get; set; }
        public string OneApiUrl { get; set; }
        public string OneApiKey { get; set; }
        public string DFPushUrl { get; set; }
        public string DFPushApiKey { get; set; }
        public string SiteServerUrl { get; set; }
    }
}