﻿namespace Specials.Domain.Models.Domain
{
    public class Event
    {
        public string Channel { get; set; }
        public long EventId { get; set; }

        public override string ToString()
        {
            return $"{nameof(Channel)} {Channel} {nameof(EventId)}:{EventId}";
        }
    }
}