﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Specials.Domain.Models.DTO;

namespace Specials.Domain.Models.Domain
{
    public class Special : SpecialDto
    {
        private string _boostFractionalPrice;
        private string _selectionStatus;
        private string _selectionDisplayStatus;

        private string _marketStatus;
        private string _marketDisplayStatus;

        private string _eventStatus;
        private string _eventDisplayStatus;

        public List<Selection> Legs { get; set; }

        public new string BoostFractionalPrice
        {
            get => _boostFractionalPrice;
            set => SetField(ref _boostFractionalPrice, value);
        }


        public new string SelectionStatus
        {
            get => _selectionStatus;
            set => SetField(ref _selectionStatus, value);
        }


        public new string SelectionDisplayStatus
        {
            get => _selectionDisplayStatus;
            set => SetField(ref _selectionDisplayStatus, value);
        }

        public new string MarketStatus
        {
            get => _marketStatus;
            set => SetField(ref _marketStatus, value);
        }


        public new string MarketDisplayStatus
        {
            get => _marketDisplayStatus;
            set => SetField(ref _marketDisplayStatus, value);
        }


        public new string EventStatus
        {
            get => _eventStatus;
            set => SetField(ref _eventStatus, value);
        }


        public new string EventDisplayStatus
        {
            get => _eventDisplayStatus;
            set => SetField(ref _eventDisplayStatus, value);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        // this applies the INotify event to each Property
        protected bool SetField<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }
    }
}