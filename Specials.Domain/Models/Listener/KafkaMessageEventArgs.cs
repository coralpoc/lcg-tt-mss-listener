﻿using System;

namespace Specials.Domain.Models.Listener
{
    public class KafkaMessageEventArgs : EventArgs
    {
        public string Message { get; set; }
        public string Channel { get; set; }
        public long EventId { get; set; }

        public override string ToString()
        {
            return $"{nameof(Channel)}:{Channel} {nameof(EventId)}:{EventId} \n {nameof(Message)}:{Message}";
        }
    }
}