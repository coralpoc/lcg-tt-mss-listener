﻿using System;
namespace Specials.Domain.Models.Listener
{
    public class ListenerEventArgs : EventArgs
    {

        public string Channel { get; set; }
        public long? SelectionId { get; set; }
        public long? MarketId { get; set; }
        public long? EventId { get; set; }
        public string SelectionStatus { get; set; }
        public string SelectionDisplayStatus { get; set; }
        public double? Price { get; set; }
        public string FractionalPrice { get; set; }
        public bool? EventIsFinished { get; set; }
        public bool? EventIsLive { get; set; }
        public bool? EventIsResulted { get; set; }
        public bool? EventIsStarted { get; set; }
        public string EventStatus { get; set; }
        public string EventDisplayStatus { get; set; }
        public string MarketStatus { get; set; }
        public string MarketDisplayStatus { get; set; }
        public bool? IsSettled { get; set; }

        public override string ToString()
        {
            return $"{nameof(Channel)}:{Channel} {nameof(EventId)}:{EventId}";
        }
    }
}
