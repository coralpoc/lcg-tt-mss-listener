﻿namespace Specials.Kafka
{
    public interface IKafkaProducer
    {
        void SendMessageToKafka(string message,long eventid);
    }
}