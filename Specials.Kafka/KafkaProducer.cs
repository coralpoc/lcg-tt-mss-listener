﻿using System;
using Confluent.Kafka;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Config = Specials.Domain.Models.Domain.Config;

namespace Specials.Kafka
{
    public class KafkaProducer : IKafkaProducer
    {
        private readonly ILogger<KafkaProducer> _logger;
        private readonly Config _config;
        private readonly ProducerConfig _producerConfig;

        public KafkaProducer(ILogger<KafkaProducer> logger, IOptions<Config> config)
        {
            _logger = logger;
            _config = config.Value;

            _producerConfig = new ProducerConfig { BootstrapServers = _config.KafkaServer };
        }

        public void SendMessageToKafka(string message, long eventId)
        {
            Action<DeliveryReport<string, string>> handler = ShowWindowsMessage;

            using var producer = new ProducerBuilder<string, string>(_producerConfig)
                .SetErrorHandler((_, e) =>
            {
                _logger.LogError($"Kafka connection error -\n" +
                                 $"Error : {e.Reason}");
            }).Build();
            producer.Produce(this._config.KafkaTopic, new Message<string, string> { Key = Convert.ToString(eventId), Value = message }, handler);
            producer.Flush(TimeSpan.FromSeconds(5));
        }

        private void ShowWindowsMessage(DeliveryReport<string, string> deliveryReport)
        {
            if (deliveryReport.Error.IsError)
            {
                _logger.LogError($"Kafka delivery error -\n" +
                                 $"Topic :{deliveryReport.TopicPartitionOffsetError.Topic} | Partition : {deliveryReport.TopicPartitionOffsetError.Partition} | " +
                                 $"Offset : {deliveryReport.TopicPartitionOffsetError.Offset}\n" +
                                 $"Error : {deliveryReport.Error.Reason}");
            }
        }
    }
}
