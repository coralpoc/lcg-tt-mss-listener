﻿using System;
using System.Net.Mime;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Specials.Domain.Models.Domain;
using Specials.Listener.Storage;
using Specials.Listener.Utilities;

namespace Specials.Listener.Service.Controllers
{
    [Route("lcg-tt-mss-listener/api/[controller]")]
    [ApiController]
    public class EventController : ControllerBase
    {
        private readonly ILogger<EventController> _logger;
        private readonly IListener _listener;
        private readonly IStorageSubscription _storage;

        public EventController( ILogger<EventController> logger, IListener listener, IStorageSubscription storage)
        {
            _logger = logger;
            _listener = listener;
            _storage = storage;
        }

        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        public IActionResult SubscribeToEvent([FromBody] Event item)
        {
            try
            {
                Task.Run(() =>
                {
                    _logger.LogInformation($"Received request from MSS {item.Channel}:{item.EventId}", LogSourceType.LogEntry);

                    _storage.PostPodNameToEvent(item.Channel,item.EventId);
                    if (!_storage.IsSubscriptionExists(item.Channel, item.EventId))
                    {
                        Thread.Sleep(2000);                       
                    }
                    _listener.ListenTo(item.Channel, item.EventId);                  
                });

                return Ok(item);
            }
            catch (Exception e)
            {
                _logger.LogError(
                    $"Unknown error attempting to subscribe to {item} Message: {e.Message} {e.StackTrace}", LogSourceType.LogEntry);
                return BadRequest();
            }
        }

        [HttpGet]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        public IActionResult GetEvent()
        {
            try
            {
                return Ok("Test Event");
            }
            catch (Exception e)
            {
                _logger.LogError($"Unknown error  Message: {e.Message} {e.StackTrace}", LogSourceType.LogEntry);
                return BadRequest();
            }
        }
    }
}