﻿using System.Net.Mime;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Specials.Domain.Models.Listener;
using Specials.Listener.Helper;
using Specials.Kafka;

namespace Specials.Listener.Service.Controllers
{
    [Route("lcg-tt-mss-listener/api/[controller]")]
    [ApiController]
    public class KafkaController : ControllerBase
    {
        private readonly IKafkaProducer _kafkaProducer;
        private readonly IListenerHelper _listenerHelper;

        public KafkaController(IKafkaProducer kafkaProducer, IListenerHelper listenerHelper)
        {
            _kafkaProducer = kafkaProducer;
            _listenerHelper = listenerHelper;
        }

        [HttpGet]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        public IActionResult SendMessageToKafka([FromBody] KafkaMessageEventArgs args)
        {
            var keyExist = _listenerHelper.FilterListenerUpdate(args.Channel, args.EventId, args.Message);
            
            return Ok($"MarketId / SelectionId Exists =  {keyExist} Message:{args.Message}");
        }
        
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        public IActionResult PostMessageToKafka([FromBody] KafkaMessageEventArgs args)
        {
            _kafkaProducer.SendMessageToKafka(JsonConvert.SerializeObject(args), args.EventId);
            return Ok();
        }
    }
}