﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Specials.Domain.Models.Domain;
using Specials.Listener.Service.Extensions;

namespace Specials.Listener.Service.Controllers
{
    [Route("lcg-tt-mss-listener/api/[controller]")]
    [ApiController]
    public class VersionController : Controller
    {
        private readonly Config _config;

        //todo need to add  end point to ingress file

        public VersionController(IOptions<Config> config)
        {
            _config = config.Value;
        }

        [HttpGet]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        public IActionResult GetBuildVersion()
        {
            return Ok(_config.ContainerImagePath.ShortFormatVersion());
        }
    }
}