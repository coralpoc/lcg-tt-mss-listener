﻿using System;

namespace Specials.Listener.Service.Extensions
{
    public static class StringExtensions
    {
        public static string ShortFormatVersion(this string version)
        {
            if (string.Equals(version, null, StringComparison.Ordinal))
            {
                return null;
            }

            var colonIndex = version.LastIndexOf(':');
            return version.Substring(colonIndex + 1, version.Length - colonIndex - 1);
        }
        public static string RemoveSpecialCharacterNewLine(this string input)
        {
            return input.Replace("\n", "");
        }
    }
}