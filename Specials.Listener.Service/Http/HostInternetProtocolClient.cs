﻿using System.Net.Http;
using System.Threading.Tasks;
using Specials.Listener.Service.Http.Interfaces;

namespace Specials.Listener.Service.Http
{
    public class HostInternetProtocolClient : IHostInternetProtocolClient
    {
        private readonly HttpClient _client;

        public HostInternetProtocolClient(HttpClient client)
        {
            _client = client;
        }

        public async Task<string> GetHostInternetProtocol()
        {
            var response = await _client.GetAsync("http://4.icanhazip.com");

            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStringAsync();
            }

            return null;
        }
    }
}