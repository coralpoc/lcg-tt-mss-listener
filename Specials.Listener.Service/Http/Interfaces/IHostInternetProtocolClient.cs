﻿using System.Threading.Tasks;

namespace Specials.Listener.Service.Http.Interfaces
{
    public interface IHostInternetProtocolClient
    {
        Task<string> GetHostInternetProtocol();
    }
}