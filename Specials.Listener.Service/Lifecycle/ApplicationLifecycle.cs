using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Specials.Domain.Models.Domain;
using Specials.Listener.Storage;
using Microsoft.Extensions.DependencyInjection;
using Specials.Listener.Utilities;

namespace Specials.Listener.Service.Lifecycle
{
    public class ApplicationLifecycle : IHostedService
    {
        private readonly IHostApplicationLifetime _lifetime;
        private readonly ILogger<ApplicationLifecycle> _logger;
        private readonly Config _config;
        private readonly IStorageSubscription _storage;
        private IListener _listener;
        private readonly IServiceProvider _serviceProvider;
        public ApplicationLifecycle(IHostApplicationLifetime lifetime,
            ILogger<ApplicationLifecycle> logger, IOptions<Config> config, IStorageSubscription storage, IListener listener, IServiceProvider serviceProvider)
        {
            _lifetime = lifetime;
            _logger = logger;
            _storage = storage;
            _listener = listener;
            _config = config.Value;
            _serviceProvider = serviceProvider;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _lifetime.ApplicationStarted.Register(OnStarted);
            _lifetime.ApplicationStopping.Register(OnStopping);

            return Task.CompletedTask;
        }


        public Task StopAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        //NOTE: This does not get called when developing locally. Works fine once deployed to AWS.
        public void OnStopping()
        {
            try
            {
                _storage.CancelSubscribedEventsOnContainer();
                _logger.LogInformation( $"Stopping Pod: {_config.ContainerName}", LogSourceType.LogEntry);
            }
            catch (Exception e)
            {
                _logger.LogError($"Unknown error whilst stopping Pod: {_config.ContainerName} Message: {e.Message} StackTrace: {e.StackTrace}", LogSourceType.LogEntry);
            }
        }

        public void OnStarted()
        {
            _logger.LogInformation( $"Starting Pod: {_config.ContainerName}", LogSourceType.LogEntry);

            var unsubscribedEvents = _storage.GetUnsubscribedEvents().ToList();

            // Add Subscriptions that have the current container name as this would indicate the pod has restarted 

            unsubscribedEvents.AddRange(_storage.GetSubscribedEventsOnContainer());

            foreach (var unsubscribedEvent in unsubscribedEvents)
            {
                _listener = _serviceProvider.GetService<IListener>();
                _storage.PostPodName(unsubscribedEvent);
                // todo this may not be required due to the reconnection service
                _listener.ListenTo(unsubscribedEvent.Channel, unsubscribedEvent.EventId);
            }
        }
    }
}