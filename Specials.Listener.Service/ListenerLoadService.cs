﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Specials.Domain.Models.Domain;
using Specials.Listener.Storage;
using Specials.Listener.Utilities;

namespace Specials.Listener.Service
{
    public class ListenerLoadService : BackgroundService
    {

        private readonly IStorageSubscription _storage;
        private readonly ILogger<ListenerLoadService> _logger;
        private readonly Config _config;

        public ListenerLoadService(IStorageSubscription storage, ILogger<ListenerLoadService> logger, IOptions<Config> config)
        {
            _storage = storage;
            _logger = logger;
            _config = config.Value;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            Task.Run(() =>
            {
                while (true)
                {
                    DistributeSubscribedEventsAcrossPods();

                    Thread.Sleep(60000);
                }

            }, stoppingToken);

            return Task.CompletedTask;
        }

        private void DistributeSubscribedEventsAcrossPods()
        {
            while (true)
            {
                var subscriptions = _storage.GetSubscribedEvents().ToList();
                var containers = _storage.GetSubscribedEventsOnContainer().ToList();

                if (!subscriptions.Any() || !containers.Any()) break;

                var load = (double)subscriptions.Count() / containers.Count();

                if (load > 0.5)
                {
                    _logger.LogInformation($"Unbalanced container load discovered on {_config.ContainerName} : Load:{load}", LogSourceType.LogEntry);
                    _storage.CancelSubscribedEventsNotStartedOnContainer(1);
                }
                else
                {
                    _logger.LogInformation($"Container balanced on {_config.ContainerName} : Load:{load}", LogSourceType.LogEntry);
                    break;
                }
            }
        }
    }
}