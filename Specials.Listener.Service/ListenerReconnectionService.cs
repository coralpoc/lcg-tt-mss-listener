﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Specials.Listener.Storage;
using Specials.Listener.Utilities;

namespace Specials.Listener.Service
{
    public class ListenerReconnectionService : BackgroundService
    {

        private readonly ILogger<ListenerReconnectionService> _logger;
        private readonly IStorageSubscription _storage;
        private readonly IListener _listener;

        public ListenerReconnectionService(ILogger<ListenerReconnectionService> logger, IStorageSubscription storage,
            IListener listener)
        {
            _logger = logger;
            _storage = storage;
            _listener = listener;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            Task.Run(() =>
            {
                while (true)
                {
                    var unsubscribedEvents = _storage.GetUnsubscribedEvents();

                    foreach (var unsubscribedEvent in unsubscribedEvents)
                    {
                        _logger.LogInformation($"Unsubscribed event found from listener Reconnection Service: {unsubscribedEvent}", LogSourceType.LogEntry);

                        _storage.PostPodName(unsubscribedEvent);
                        _listener.ListenTo(unsubscribedEvent.Channel, unsubscribedEvent.EventId);
                    }

                    _logger.LogInformation($"Waiting 30 secs for listener Reconnection Service", LogSourceType.LogEntry);
                    Thread.Sleep(30000);
                }

            }, stoppingToken);

            return Task.CompletedTask;
        }
    }
}