﻿using System.Threading.Tasks;

namespace Specials.Listener.Service.Logger
{
    public interface INLogConfigRunner
    {
        Task Initialise();
    }
}