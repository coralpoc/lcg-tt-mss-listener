﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NLog;
using Specials.Domain.Models.Domain;
using Specials.Listener.Service.Extensions;
using Specials.Listener.Service.Http.Interfaces;
using LogLevel = Microsoft.Extensions.Logging.LogLevel;

namespace Specials.Listener.Service.Logger
{
    public class NLogConfigRunner : INLogConfigRunner
    {
        private readonly Config _config;
        private readonly IHostInternetProtocolClient _client;
        private readonly ILogger<NLogConfigRunner> _logger;

        public NLogConfigRunner(IOptions<Config> config, IHostInternetProtocolClient client, ILogger<NLogConfigRunner> logger)
        {
            _client = client;
            _logger = logger;
            _config = config.Value;
        }

        public async Task Initialise()
        {
            var hostPublicIp = await _client.GetHostInternetProtocol();
            LogManager.Configuration = NLogConfiguration.Base;

            NLogConfiguration.AddAsyncFlatFileTargets(_config.App, GetMinimumLogLevel(), _config.LogFilePath, _config.LogFileSizeLimit);
            NLogConfiguration.SetPublicIp(hostPublicIp.RemoveSpecialCharacterNewLine());
            NLogConfiguration.SetEnvironment(_config.Environment);
            NLogConfiguration.SetAppName(_config.App);
            NLogConfiguration.SetKubernetesContexts(_config.HostName, _config.ContainerName, _config.ContainerImagePath,
                _config.PrivateIp,
                _config.ContainerIp);

            _logger.LogInformation( $"Configuration: MinimumLogLevel is {GetMinimumLogLevel()}");
        }

        private LogLevel GetMinimumLogLevel()
        {
            Enum.TryParse(typeof(LogLevel), _config.MinimumLoggingLevel, out var minimumLogLevel);
            return (LogLevel) minimumLogLevel;
        }
    }
}