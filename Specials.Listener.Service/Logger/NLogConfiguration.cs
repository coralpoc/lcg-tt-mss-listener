﻿using System.Linq;
using NLog;
using NLog.Common;
using NLog.Config;
using NLog.Layouts;
using NLog.Targets;
using NLog.Targets.Wrappers;
using LogLevel = Microsoft.Extensions.Logging.LogLevel;

namespace Specials.Listener.Service.Logger
{
    public static class NLogConfiguration
    {
        #region Private Members 

        /// <summary>
        ///     NLogConfiguration encapsulates the creation/removal of NLog targets and rules
        /// </summary>
        static NLogConfiguration()
        {
            // LoadFrameworkConfiguration NLog internal log
            InternalLogger.LogFile = "internal.log";
            InternalLogger.LogLevel = NLog.LogLevel.Off;           
            // Register File target
            LogManager.Setup().SetupExtensions(ext => ext.RegisterTarget<FileTarget>("LogFile"));

            var loggingConfiguration = new LoggingConfiguration();

            // Add flat file target
            var fileTarget = AsyncFlatFileTarget;
            loggingConfiguration.AddTarget(fileTarget);

            //Removing add rules to not have duplicate log files created in the app folder
            // Add rules
            //loggingConfiguration.LoggingRules.Add(new LoggingRule("*", NLog.LogLevel.Warn, fileTarget));

            // Set Base configuration
            Base = loggingConfiguration;
        }

        /// <summary>
        ///     An new asynchronous target.
        /// </summary>
        private static AsyncTargetWrapper AsyncFlatFileTarget => new AsyncTargetWrapper
        {
            Name = "asyncFlatFile",
            OverflowAction = AsyncTargetWrapperOverflowAction.Grow,
            TimeToSleepBetweenBatches = 0,
            WrappedTarget = new FileTarget()
            {
                Name = "file",
                Layout = LogLayout,
                ArchiveAboveSize = LogFileSizeLimit,
                FileName = $"{LogFilePath}{AppName}" + "-logFile-${shortDate}.log",
                MaxArchiveFiles = MaxArchiveFiles
            }
        };

        private static AsyncTargetWrapper AsyncBackgroundTaskFlatFileTarget => new AsyncTargetWrapper
        {
            Name = "asyncBackgroundTaskFlatFile",
            OverflowAction = AsyncTargetWrapperOverflowAction.Grow,
            TimeToSleepBetweenBatches = 0,
            WrappedTarget = new FileTarget
            {
                Name = "backgroundTaskFlatFile",
                Layout = LogLayout,
                ArchiveAboveSize = LogFileSizeLimit,
                FileName = $"{LogFilePath}{AppName}" + "-logFile-${shortDate}.log",
                MaxArchiveFiles = MaxArchiveFiles
            }
        };

        /// <summary>
        ///     The destination queue name.
        /// </summary>
        private static string AppName { get; set; }


        /// <summary>
        ///     A new log layout.
        /// </summary>
        private static JsonLayout LogLayout => new JsonLayout
        {
            Attributes =
            {
                new JsonAttribute
                {
                    Name = "timestamp",
                    Layout =
                        @"${date:universalTime=true:format=yyyy-MM-ddTHH\:mm\:ss.fffZ}"
                },
                new JsonAttribute
                    {Name = "level", Layout = "${level:uppercase=true}"},
                new JsonAttribute {Name = "msg", Layout = "${message}"},
                new JsonAttribute
                    {Name = "exception", Layout = "${exception:format=@}"},
                new JsonAttribute
                {
                    Name = "correlationId",
                    Layout = "${event-properties:item=correlationId}"
                },
                new JsonAttribute
                {
                    Name = "transactionId",
                    Layout = "${event-properties:item=transactionId}"
                },
                new JsonAttribute
                    {Name = "traceId", Layout = "${aspnet-TraceIdentifier}"},
                new JsonAttribute
                {
                    Name = "eventType",
                    Layout = "${event-properties:item=eventType}"
                },
                new JsonAttribute
                {
                    Name = "metricType",
                    Layout = "${event-properties:item=metricType}"
                },
                new JsonAttribute
                {
                    Name = "metricValue",
                    Layout = "${event-properties:item=metricValue}",
                    Encode = false
                },
                new JsonAttribute {Name = "logger", Layout = "${logger}"},
                new JsonAttribute
                    {Name = "hostName", Layout = "${gdc:item=hostName}"},
                new JsonAttribute
                    {Name = "containerName", Layout = "${gdc:item=containerName}"},
                new JsonAttribute
                    {Name = "hostPublicIp", Layout = "${gdc:item=publicIP}"},
                new JsonAttribute
                    {Name = "hostPrivateIp", Layout = "${gdc:item=privateIP}"},
                new JsonAttribute
                    {Name = "containerIp", Layout = "${gdc:item=containerIP}"},
                new JsonAttribute
                    {Name = "containerId", Layout = "${machinename}"},
                new JsonAttribute
                    {Name = "containerPath", Layout = "${gdc:item=containerPath}"},
                //new JsonAttribute
                //    {Name = "appName", Layout = "${environment:APP_NAME}"},
                new JsonAttribute
                    {Name = "app", Layout = "${gdc:item=appName}"},
                new JsonAttribute
                    {Name = "appRole", Layout = "${environment:APP_ROLE}"},
                new JsonAttribute
                    {Name = "appSHA", Layout = "${environment:APP_SHA1}"},
                new JsonAttribute
                    {Name = "environment", Layout = "${gdc:item=environment}"},
                new JsonAttribute
                    {Name = "product", Layout = "${environment:SPECIALS_PRODUCT}"},
                new JsonAttribute
                    {Name = "clientIp", Layout = "${aspnet-request-ip}"},
                new JsonAttribute {Name = "threadId", Layout = "${threadid}"},
                new JsonAttribute {Name = "source", Layout = "Trading"},
                new JsonAttribute {Name = "channel", Layout = "${gdc:item=brand}"}
            }
        };

        /// <summary>
        ///     A new background task log layout.
        /// </summary>
        private static JsonLayout BackgroundTaskLogLayout
        {
            get
            {
                var backgroundLayout = LogLayout;

                // replace aspnet-TraceIdentifier with an event-property
                var attribute = backgroundLayout.Attributes.FirstOrDefault(e => e.Name == "traceId");
                var index = backgroundLayout.Attributes.IndexOf(attribute);
                backgroundLayout.Attributes[index] = new JsonAttribute
                { Name = "traceId", Layout = "${event-properties:item=traceId}" };

                // remove aspnet-request-ip attribute
                attribute = backgroundLayout.Attributes.FirstOrDefault(e => e.Name == "clientIp");
                backgroundLayout.Attributes.Remove(attribute);

                return backgroundLayout;
            }
        }

        #endregion

        #region Public members

        /// <summary>
        ///     The base LoggingConfiguration to be used to be used when instantiating NLog.
        /// </summary>
        public static LoggingConfiguration Base { get; }
        public static string LogFilePath { get; internal set; }
        public static long LogFileSizeLimit { get; internal set; }
        public static int MaxArchiveFiles { get; internal set; }

        public static void AddAsyncFlatFileTargets(string appName, LogLevel minimumLogLevel)
        {
            AppName = appName;

            // add main logging target
            var target = AsyncFlatFileTarget;
            LogManager.Configuration.AddTarget(target);

            // add background tasks target
            var backgroundTaskTarget = AsyncBackgroundTaskFlatFileTarget;
            LogManager.Configuration.AddTarget(backgroundTaskTarget);

            LogManager.Configuration.LoggingRules.Add(new LoggingRule("*", ConvertLogLevel(minimumLogLevel), target));          
            LogManager.ReconfigExistingLoggers();
        }

        /// <summary>
        ///     Sets the public ip of the ec2
        /// </summary>
        /// <param name="publicIp"></param>
        public static void SetPublicIp(string publicIp)
        {
            GlobalDiagnosticsContext.Set("publicIP", publicIp);
        }

        /// <summary>
        ///     Sets the private ip of the ec2
        /// </summary>
        /// <param name="privateIp"></param>
        public static void SetPrivateIp(string privateIp)
        {
            GlobalDiagnosticsContext.Set("privateIP", privateIp);
        }

        /// <summary>
        ///     Set the environment value to GlobalDiagnosticsContext
        /// </summary>
        /// <param name="environment">Environment</param>
        public static void SetEnvironment(string environment)
        {
            GlobalDiagnosticsContext.Set("environment", environment);
        }

        public static void SetAppName(string appName)
        {
            GlobalDiagnosticsContext.Set("appName", appName);
        }

        public static void SetBrand(string brand)
        {
            GlobalDiagnosticsContext.Set("brand", brand);
        }

        /// <summary>
        ///     Sets the Kubernetes Global Diagnostic Contexts, so that hosting context is logged with each log event.
        /// </summary>
        /// <param name="hostName">The Kubernetes node (host) name.</param>
        /// <param name="containerName">The Kubernetes pod name.</param>
        /// <param name="containerPath">The Docker image name.</param>
        /// <param name="privateIp">The Kubernetes pod private ip.</param>
        /// <param name="containerIp">The container ip.</param>
        public static void SetKubernetesContexts(string hostName, string containerName, string containerPath,
            string privateIp, string containerIp)
        {
            // Set k8s metadata as global logging contexts
            GlobalDiagnosticsContext.Set("hostId", hostName);
            GlobalDiagnosticsContext.Set("taskId", containerName);
            GlobalDiagnosticsContext.Set("containerPath", containerPath);
            GlobalDiagnosticsContext.Set("privateIP", privateIp);
            GlobalDiagnosticsContext.Set("containerIP", containerIp);
        }


        /// <summary>
        ///     Converts the log level.
        /// </summary>
        /// <param name="logLevel">The log level.</param>
        /// <returns>NLog Log Level.</returns>
        private static NLog.LogLevel ConvertLogLevel(LogLevel logLevel)
        {
            switch (logLevel)
            {
                case LogLevel.Trace:
                    {
                        return NLog.LogLevel.Trace;
                    }
                case LogLevel.Debug:
                    {
                        return NLog.LogLevel.Debug;
                    }
                case LogLevel.Information:
                    {
                        return NLog.LogLevel.Info;
                    }
                case LogLevel.Warning:
                    {
                        return NLog.LogLevel.Warn;
                    }
                case LogLevel.Error:
                    {
                        return NLog.LogLevel.Error;
                    }
                case LogLevel.Critical:
                    {
                        return NLog.LogLevel.Fatal;
                    }
                case LogLevel.None:
                    {
                        return NLog.LogLevel.Off;
                    }
                default:
                    {
                        return NLog.LogLevel.Debug;
                    }
            }
        }

        #endregion
    }

    /// <summary>
    ///     The type of item logged for the purpose of monitoring.
    /// </summary>
    public enum LogSourceType
    {
        Metric,
        LogEntry
    }


}