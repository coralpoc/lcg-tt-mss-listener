using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using NLog.Web;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore;
using Microsoft.Extensions.DependencyInjection;

namespace Specials.Listener.Service
{
    public class Program
    {
        /// <summary>
        ///     Load configuration from environment variables
        /// </summary>
        public static IConfiguration Config = new ConfigurationBuilder().AddEnvironmentVariables().Build();

        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                .ConfigureLogging(logging =>
                {
                    logging.ClearProviders();
                    logging.SetMinimumLevel(
                        Config.GetValue<LogLevel>("SPECIALS_MINIMUM_LOGGING_LEVEL"));
                })
                .UseNLog()
                .UseStartup<Startup>();
        }
    }
}
