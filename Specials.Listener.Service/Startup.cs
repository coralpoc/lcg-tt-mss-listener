using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog;
using Specials.Domain.Models.Domain;
using Specials.Listener.Helper;
using Specials.Listener.Http;
using Specials.Listener.Service.Http;
using Specials.Listener.Service.Http.Interfaces;
using Specials.Listener.Service.Lifecycle;
using Specials.Listener.Service.Logger;
using Specials.Listener.Storage;
using Specials.Kafka;
using Specials.RedisStorage;
using StackExchange.Redis;
using System;
using System.Net;
using System.Net.Http;
using LogLevel = Microsoft.Extensions.Logging.LogLevel;

namespace Specials.Listener.Service
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public ILogger<Startup> _logger;

        #region Private Variables 
        private static string _environment;
        #endregion

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            ConfigureAppSettings(services);

            services.AddControllers();

            services.AddTransient<ICancelEventSubscription, CancelEventSubscription>();

            services.AddTransient<IStorageSubscription, StorageSubscription>();
            services.AddTransient<IListenerHelper, ListenerHelper>();
            services.AddTransient<IListener, Listener>();
            services.AddTransient<IKafkaProducer, KafkaProducer>();

            services.AddHttpClient<IListenerHttpClient, ListenerHttpClient>().ConfigurePrimaryHttpMessageHandler(() =>
            new HttpClientHandler()
            {
                Proxy = new WebProxy(Configuration.GetValue<string>("PROXY_URI")),
                UseProxy = true,
            });


            
            services.AddHttpClient<IHostInternetProtocolClient, HostInternetProtocolClient>();
            services.AddSingleton<ILoggerFactory, LoggerFactory>();
            services.AddLogging(builder =>
            {
                builder.AddFilter("Microsoft", LogLevel.Error);
                builder.AddFilter("System", LogLevel.Error);
            });

            services.AddSingleton<IRedisStorage, RedisStorage.RedisStorage>(
                x => new RedisStorage.RedisStorage(
                    Configuration.GetValue<string>("REDIS_CLUSTER_CONN"),
                    Configuration.GetValue<int>("REDIS_CACHE_EXPIRY_TIME"),
                    Configuration.GetValue<int>("REDIS_DB_NAME"),
                    Configuration.GetValue<int>("KEEPALIVE"),
                    Configuration.GetValue<int>("CONNECTTIMEOUT"),
                    Configuration.GetValue<int>("SYNCTIMEOUT")
                    ));

            //services.AddHostedService<ListenerReconnectionService>();
            //services.AddHostedService<ListenerLoadService>();
            services.AddHostedService<ApplicationLifecycle>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILogger<Startup> logger)
        {
            _logger = logger;

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            _environment = Configuration.GetValue<string>("SPECIALS_ENVIRONMENT");
            if (string.IsNullOrEmpty(_environment))
            {
                _environment = "local";
            }
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

           
            InitNLog();
        }

        private void InitNLog()
        {
            LogManager.Configuration = NLogConfiguration.Base;

            TryGetConfigValue<LogLevel>("SPECIALS_MINIMUM_LOGGING_LEVEL", out var minimumLogLevel);
            TryGetConfigValue<string>("SPECIALS_LOGGING_QUEUE_NAME", out var loggingQueueName);
            TryGetConfigValue<string>("APP", out var app);
            TryGetConfigValue<string>("LOGFILE_PATH", out string logFilePath);
            TryGetConfigValue<long>("LOGFILE_SIZE_LIMIT", out long logFileSizeLimit);
            TryGetConfigValue<int>("MAX_ARCHIVE_FILES", out int maxArchiveFiles);
            TryGetConfigValue<string>("BRAND", out string brand);
            NLogConfiguration.LogFilePath = logFilePath;
            NLogConfiguration.LogFileSizeLimit = logFileSizeLimit;
            NLogConfiguration.MaxArchiveFiles = maxArchiveFiles;

            NLogConfiguration.AddAsyncFlatFileTargets(app, minimumLogLevel);

            _logger.Log(LogLevel.Information, $"Configuration: MinimumLogLevel is {minimumLogLevel}");

            NLogConfiguration.SetEnvironment(_environment);
            NLogConfiguration.SetAppName(app);
            NLogConfiguration.SetBrand(brand);

            // If the HOST_NAME & CONTAINER_NAME env vars are set then we're in K8 context 
            if (TryGetConfigValue("HOST_NAME", out string hostName) &&
                TryGetConfigValue("CONTAINER_NAME", out string containerName) &&
                TryGetConfigValue("CONTAINER_IMAGE_PATH", out string containerImagePath) &&
                TryGetConfigValue("PRIVATE_IP", out string privateIp) &&
                TryGetConfigValue("CONTAINER_IP", out string containerIp))
            {
                NLogConfiguration.SetKubernetesContexts(hostName, containerName, containerImagePath, privateIp,
                                                        containerIp);
            }
        }

        /// <summary>
        ///     Loads configuration value from config.
        /// </summary>
        /// <typeparam name="T">The type of configuration parameter.</typeparam>
        /// <param name="name">The configuration parameter name.</param>
        /// <param name="value">The configuration value out parameter.</param>
        /// <returns>A boolean indicating success of the method.</returns>
        private bool TryGetConfigValue<T>(string name, out T value)
        {
            var loaded = false;
            value = default(T);

            try
            {
                var configValue = Configuration.GetValue<string>(name);

                if (!string.IsNullOrWhiteSpace(configValue))
                {
                    value = ChangeType<T>(configValue, typeof(T));
                    loaded = true;

                    _logger.LogInformation($"{name} - {configValue} loaded from config.", LogSourceType.LogEntry);
                }
                else
                {
                    _logger.LogInformation($"Configuration setting {name} not specified.", LogSourceType.LogEntry);
                }
            }
            catch
            {
                _logger.LogCritical($"Configuration setting {name} not specified or invalid.");
            }

            return loaded;
        }

        /// <summary>
        ///     Changes the type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The value.</param>
        /// <param name="type">The type.</param>
        /// <returns>Value as expected type.</returns>
        private static T ChangeType<T>(string value, Type type)
        {
            if (type.IsEnum)
            {
                Enum.TryParse(type, value, out var result);
                return (T)result;
            }

            return (T)Convert.ChangeType(value, type);
        }

        public void ConfigureAppSettings(IServiceCollection services)
        {
            services.Configure<Config>(config =>
            {
                config.ContainerImagePath = Configuration.GetValue<string>("CONTAINER_IMAGE_PATH");
                config.RedisCluster = Configuration.GetValue<string>("REDIS_CLUSTER_CONN");
                config.RedisExpiry = Configuration.GetValue<string>("REDIS_CACHE_EXPIRY_TIME");
                config.RedisDbName = Configuration.GetValue<string>("REDIS_DB_NAME");
                config.ContainerName = Configuration.GetValue<string>("CONTAINER_NAME");

                config.MinimumLoggingLevel = Configuration.GetValue<string>("SPECIALS_MINIMUM_LOGGING_LEVEL");
                config.Environment = Configuration.GetValue<string>("SPECIALS_ENVIRONMENT");
                config.BaseUrl = Configuration.GetValue<string>("BASE_URL");

                config["ld"].DFPushUrl = Configuration.GetValue<string>("DFPUSH_LD_URL");
                config["ld"].DFPushApiKey = Configuration.GetValue<string>("DFPUSH_LD_APIKEY");

                config["cd"].DFPushUrl = Configuration.GetValue<string>("DFPUSH_CD_URL");
                config["cd"].DFPushApiKey = Configuration.GetValue<string>("DFPUSH_CD_APIKEY");

                config.KafkaServer = Configuration.GetValue<string>("KAFKA_SERVER");
                config.KafkaTopic = Configuration.GetValue<string>("KAFKA_TOPIC");
                config.SubscriptionCleanupTime = Configuration.GetValue<string>("SUBSCRIPTION_CLEANUP_TIME");
                config.SubscriptionRefreshTime = Configuration.GetValue<string>("SUBSCRIPTION_REFRESH_TIME");
                config.App = Configuration.GetValue<string>("APP");
                config.LogFilePath = Configuration.GetValue<string>("LOGFILE_PATH");
                config.LogFileSizeLimit = Configuration.GetValue<long>("LOGFILE_SIZE_LIMIT");

                config.ConnectTimeout = Configuration.GetValue<string>("KEEPALIVE");
                config.KeepAlive = Configuration.GetValue<string>("CONNECTTIMEOUT");
                config.SyncTimeout = Configuration.GetValue<string>("SYNCTIMEOUT");

                config.listenerhttpclientConnectionTimeout = Configuration.GetValue<double>("DF_PUSH_CONNECTION_TIMEOUT");
                config.listenerhttpclientRetrycount = Configuration.GetValue<int>("DF_PUSH_RETRY_COUNT");
                config.listenerhttpclientRetrydurtaion = Configuration.GetValue<double>("DF_PUSH_RETRY_DURATION");

            });
        }
    }
}
