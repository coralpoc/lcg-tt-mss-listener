﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Specials.Domain.Models.Domain;
using Specials.Listener.Storage;
using Specials.Listener.Utilities;

namespace Specials.Listener
{
    public class CancelEventSubscription : ICancelEventSubscription
    {
        public delegate void CancelEventHandler(object sender, bool args);

        public event CancelEventHandler CancelReceived;

        private readonly ILogger<CancelEventSubscription> _logger;
        private readonly IStorageSubscription _storage;
        private readonly Config _config;

        public CancelEventSubscription(ILogger<CancelEventSubscription> logger, IStorageSubscription storage, IOptions<Config> config)
        {
            _logger = logger;
            _storage = storage;
            _config = config.Value;
        }

        public void ValidateEventSubscriptionAsync(string channel, long eventId)
        {
            Task.Run(() =>
            {
                while (true)
                {

                    Thread.Sleep(30000);

                    var subscribedEvent = _storage.GetSubscribedEvent(channel, eventId);

                    if (subscribedEvent == null)
                    {
                        _logger.LogInformation($"Event no longer exists is Cache ch:{channel}:e:{eventId}",LogSourceType.LogEntry);
                        CancelReceived?.Invoke(this, true);
                        break;
                    }

                    _logger.LogInformation($"Waiting 5 secs before validating Event in Cache ch:{channel}:e:{eventId}", LogSourceType.LogEntry);
                    Thread.Sleep(5000);
                }
            });
        }

    }
}