﻿namespace Specials.Listener.Helper
{
    public interface IListenerHelper
    {
        bool FilterListenerUpdate(string channel, long eventId, string message);
    }
}