﻿using System;
using System.Linq;
using Newtonsoft.Json.Linq;
using Specials.RedisStorage;

namespace Specials.Listener.Helper
{
    public class ListenerHelper : IListenerHelper
    {
        private readonly IRedisStorage _redisStorage;

        public ListenerHelper(IRedisStorage redisStorage)
        {
            _redisStorage = redisStorage;
        }

        public bool FilterListenerUpdate(string channel, long eventId, string message)
        {
            var jsonStart = message.IndexOf("data: ", StringComparison.Ordinal) + "data: ".Length;
            var json = message.Substring(jsonStart);
          
            dynamic jObjectMessage = JObject.Parse(json);

            string partialKey;
            var isKeyExists = true;

            if (jObjectMessage.market != null)
            {
                var marketKey = (string) jObjectMessage.market.marketKey;
                partialKey = $"leg:ch:{channel}:e:{eventId}:m:{marketKey}:";               
                isKeyExists = _redisStorage.GetKeys(partialKey).Any();
            }

            if (jObjectMessage.selection != null)
            {
                var parent = (string) jObjectMessage.selection.meta.parents;
                var marketIdIndex = parent.IndexOf("m.", StringComparison.Ordinal);
                var marketId = parent.Substring(marketIdIndex).Replace("m.", string.Empty);

                var selectionKey = (string) jObjectMessage.selection.selectionKey;
                var rule4Key = jObjectMessage.selection.rule4 != null ? (string)jObjectMessage.selection.rule4.rule4Id : null;
                if (rule4Key!=null)
                {
                    selectionKey = null;
                    partialKey = $"leg:ch:{channel}:e:{eventId}:m:{marketId}:s:{selectionKey}";
                    isKeyExists = _redisStorage.GetKeys(partialKey).Any();
                }
                else
                {
                    partialKey = $"leg:ch:{channel}:e:{eventId}:m:{marketId}:s:{selectionKey}";
                    isKeyExists = _redisStorage.GetKeys(partialKey).Any();
                }
               
                
            }

            return isKeyExists;
        }
    }
}