﻿using System.Threading;
using System.Threading.Tasks;

namespace Specials.Listener.Http
{
    public interface IListenerHttpClient
    {
         Task ListenAsync(string channel, long eventId, CancellationToken tokenToken);

        event ListenerHttpClient.MessageReceivedHandler MessageReceived;
        event ListenerHttpClient.DisconnectEventHandler Disconnect;
        event ListenerHttpClient.RefreshEventHandler RefreshSubscription;
    }
}