﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Specials.Domain.Models.Domain;
using Specials.Domain.Models.Listener;
using Specials.Listener.Utilities;
using Microsoft.Extensions.Logging;
using Polly;
using StackExchange.Redis;
using System.Net.Sockets;
using System.Timers;

namespace Specials.Listener.Http
{
    public class ListenerHttpClient : IListenerHttpClient
    {
        public delegate void MessageReceivedHandler(object sender, KafkaMessageEventArgs args);

        public delegate void DisconnectEventHandler(object sender, ListenerEventArgs args);

        public delegate void RefreshEventHandler(object sender, string channel, long eventId, Guid subscriptionId);

        public event MessageReceivedHandler MessageReceived;
        public event DisconnectEventHandler Disconnect;
        public event RefreshEventHandler RefreshSubscription;

        private readonly Config _config;

        private readonly ILogger<ListenerHttpClient> _logger;
        private readonly HttpClient _httpClient;
        public ListenerHttpClient(ILogger<ListenerHttpClient> logger, IOptions<Config> config, HttpClient httpClient)
        {
            _logger = logger;
            _httpClient = httpClient;
            _config = config.Value;
        }

        public async Task ListenAsync(string channel, long eventId, CancellationToken token)
        {
            var subscriptionId = Guid.NewGuid();

            try
            {
                _logger.LogInformation($"Subscribing to listener ch:{channel}:e:{eventId}\nGUID: {subscriptionId}", LogSourceType.LogEntry);  

                var maxRetryAttempts = _config.listenerhttpclientRetrycount;
                var pauseBetweenFailures = TimeSpan.FromSeconds(_config.listenerhttpclientRetrydurtaion);

                var retryPolicy = Policy.Handle<OperationCanceledException>()
                    .Or<HttpRequestException>().WaitAndRetryAsync(maxRetryAttempts, i => pauseBetweenFailures, onRetry: (outcome, timespan, retryAttempt, context) =>
                {
                    _logger.LogWarning("Delaying for {delay}ms, then making retry {retry} for event Id " + eventId, timespan.TotalMilliseconds, retryAttempt);
                });

                HttpResponseMessage response = null;
                await retryPolicy.ExecuteAsync(async () =>
                {
                    var request = new HttpRequestMessage(HttpMethod.Get, _config[channel].DFPushUrl + "/events/" + eventId)
                    {
                        Version = HttpVersion.Version10,
                    };
                    _httpClient.DefaultRequestHeaders.Clear();
                    _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/event-stream"));                   
                    var stopwatch = new Stopwatch();
                    stopwatch.Start();
                    response =
                   await _httpClient.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);
                    stopwatch.Stop();
                    _logger.LogInformation($"Header :{_httpClient.DefaultRequestHeaders.Accept} \n Request URI :{request.RequestUri} \nSubscribing to Listener ch:{channel}:e:{eventId}\nGUID: {subscriptionId}\nStatus Code:{response.StatusCode}", LogSourceType.LogEntry);
                    response.EnsureSuccessStatusCode();
                });

                if (response != null && response.IsSuccessStatusCode)
                {                 
                    await Task.Run(async () => { await StreamResponseAsync(response, channel, eventId, subscriptionId, token); }, token);                  
                }

            }

            catch (OperationCanceledException oc)
            {
                _logger.LogError(
                    $"Operation Cancelled: ch:{channel}:e:{eventId} \nMessage: {oc.Message}\nGUID:{subscriptionId}\nStack Trace: {oc.StackTrace}", LogSourceType.LogEntry);
            }

            catch (TimeoutException e)
            {
                _logger.LogError(
                    $"Timeout Error: ch:{channel}:e:{eventId} \nMessage:{e.Message} {e.StackTrace}\nGUID:{subscriptionId}", LogSourceType.LogEntry);
            }
            catch (ObjectDisposedException od)
            {
                _logger.LogError(
                    $"Object disposed exception  ch:{channel}:e:{eventId} Error: {od.Message} Message {od.StackTrace} {od.InnerException}\nGUID: {subscriptionId}", LogSourceType.LogEntry);

                DisconnectService(channel, eventId);
            }
            catch (HttpRequestException httpreq)
            {
                _logger.LogError($"Http Response message :{httpreq.StackTrace} \n Header:{ _httpClient.DefaultRequestHeaders.Accept} \n Request URI: {_config[channel].DFPushUrl + "/events/" + eventId}\nGUID: {subscriptionId} \nStatus Code:{httpreq.StatusCode}", LogSourceType.LogEntry);
            }
            catch (Exception e)
            {

                _logger.LogError(
                    $"Unknown Error thrown in Listener Listener ch:{channel}:e:{eventId} Error: {e.StackTrace} Message {e.Message}\nGUID: {subscriptionId}", LogSourceType.LogEntry);

                DisconnectService(channel, eventId);
            }
        }

        private async Task StreamResponseAsync(HttpResponseMessage response, string channel, long eventId, Guid subscriptionId,
            CancellationToken token)
        {
            try
            {
                _logger.LogInformation(
                    $"Subscribed to Listener ch:{channel}:e:{eventId}\nGUID: {subscriptionId}", LogSourceType.LogEntry);

                string line = null;
                if (response != null)
                {
                    await using var stream = await response.Content.ReadAsStreamAsync();
                    Stopwatch sw = new Stopwatch();
                    int lineCount = 0;
                    using (StreamReader reader = new StreamReader(stream))
                    {                        
                        while (true)
                        {                          
                            token.ThrowIfCancellationRequested();
                           // _logger.LogInformation($"ThrowIfCancellationRequested for ch:{channel}:e:{eventId}\nGUID: {subscriptionId}", LogSourceType.LogEntry);
                            try
                            {
                                var refreshtask = reader.ReadLineAsync();                               
                                if (await Task.WhenAny(
                                            refreshtask,
                                            Task.Delay(TimeSpan.FromSeconds(300))) == refreshtask)
                                {
                                   line = refreshtask.Result;
                                }
                                else
                                {
                                     //throw new TimeoutException();
                                    _logger.LogInformation($"ReadlineAsync takes more than 5 mins - ch:{channel}:e:{eventId}\nGUID: {subscriptionId}", LogSourceType.LogEntry);
                                    RefreshSubscription?.Invoke(this, channel, eventId, subscriptionId);
                                    break;
                                }
                            }                            
                            //catch (TimeoutException ex)
                            //{
                            //    _logger.LogError($"TimeOut Exception at ReadlineAsync - ch:{channel}:e:{eventId}\nGUID: {subscriptionId} Message {ex.Message} StackTrace {ex.StackTrace}", LogSourceType.LogEntry);                                
                            //    RefreshSubscription?.Invoke(this, channel, eventId, subscriptionId);
                            //    break;
                            //}
                            catch (Exception ex)
                            {
                               // _logger.LogError($"Exception at ReadlineAsync for ch:{channel}:e:{eventId}\nGUID: {subscriptionId} Message {ex.Message} StackTrace {ex.StackTrace}", LogSourceType.LogEntry);
                                 RefreshSubscription?.Invoke(this, channel, eventId, subscriptionId);                               
                                break;
                            }
                            _logger.LogInformation($"line for ch:{channel}:e:{eventId}\nGUID: {subscriptionId}\nLine: {line}", LogSourceType.LogEntry);                            
                            if (string.IsNullOrEmpty(line) || line == ":empty" || line.StartsWith("id"))
                            {
                                if(string.IsNullOrEmpty(line) || line == ":empty")
                                {
                                    sw.Start();
                                    lineCount++;
                                    if (sw.ElapsedMilliseconds > 360000 || lineCount > 3)// fire every 6 minutes or empty line count more than 3
                                    {
                                       sw.Stop();
                                       lineCount = 0;
                                       if(lineCount > 3)
                                        {
                                           _logger.LogInformation($"reaches max readline count ch:{channel}:e:{eventId}\nGUID: {subscriptionId}\nLine: {line}", LogSourceType.LogEntry);
                                        }
                                        else
                                        {
                                            _logger.LogInformation($"reaches max readlineAsync ch:{channel}:e:{eventId}\nGUID: {subscriptionId}\nLine: {line}", LogSourceType.LogEntry);
                                        }

                                       RefreshSubscription?.Invoke(this, channel, eventId, subscriptionId);
                                    }
                                }
                                else
                                {                                  
                                    sw.Reset();
                                    lineCount = 0;

                                }
                                continue;
                            }
                            sw.Reset();
                            MessageReceived?.Invoke(this,
                                  new KafkaMessageEventArgs() { Channel = channel, EventId = eventId, Message = line });
                            _logger.LogInformation($"MessageReceived for ch:{channel}:e:{eventId}\nGUID: {subscriptionId}\nLine: {line}", LogSourceType.LogEntry);

                        }
                    }

                }
                else
                {
                    _logger.LogInformation($"Null Response for ch:{channel}:e:{eventId}\nGUID: {subscriptionId}", LogSourceType.LogEntry);
                    RefreshSubscription?.Invoke(this, channel, eventId, subscriptionId);                  
                }
            }

            catch (OperationCanceledException oce)
            {
                if (oce.CancellationToken.IsCancellationRequested)
                {
                    _logger.LogError(
                    $"Cancellation token received for ch:{channel}:e:{eventId}\nGUID: {subscriptionId} Stack Trace {oce.StackTrace} message {oce.Message}", LogSourceType.LogEntry);
                }
                else
                {
                    _logger.LogError($"Opeartion Cancelled for ch:{channel}:e:{eventId}\nGUID: {subscriptionId} . Error: {oce.Message} StackTrace: {oce.StackTrace}");
                }
            }
            catch (IOException ex)
            {
                _logger.LogError($"IO Exception - ch:{channel}:e:{eventId}\nGUID: {subscriptionId} Message {ex.Message} StackTrace {ex.StackTrace}", LogSourceType.LogEntry);
                _logger.LogInformation($"Refreshing subscription for ch:{channel}:e:{eventId}\nGUID: {subscriptionId}");
                RefreshSubscription?.Invoke(this, channel, eventId, subscriptionId);
                
            }
            catch (TimeoutException ex)
            {
                _logger.LogError($"TimeOut Exception - ch:{channel}:e:{eventId}\nGUID: {subscriptionId} Message {ex.Message} StackTrace {ex.StackTrace}", LogSourceType.LogEntry);
                _logger.LogInformation($"Refreshing subscription for ch:{channel}:e:{eventId}\nGUID: {subscriptionId}");
                RefreshSubscription?.Invoke(this, channel, eventId, subscriptionId);
                
            }
            catch (Exception ex)
            {
                _logger.LogError(
                    $"EventId: {eventId} \nUnable to StreamResponseAsync {ex.StackTrace} Message: {ex.Message}\nGUID: {subscriptionId}", LogSourceType.LogEntry);
                _logger.LogInformation(
                    $"CANCELLED SUBSCRIPTION by disposing the stream reader due to refresh cancellation token received for ch:{channel}:e:{eventId}\nGUID: {subscriptionId}", LogSourceType.LogEntry);
                RefreshSubscription?.Invoke(this, channel, eventId, subscriptionId);
               
            }
            finally
            {
                _logger.LogInformation(
                    $"Response disposed for ch:{channel}:e:{eventId}\nGUID: {subscriptionId}", LogSourceType.LogEntry);
                Thread.Sleep(1000);
                response.Dispose();
            }
        }   
        
        private void DisconnectService(string channel, long eventId)
        {
            try
            {
                Disconnect?.Invoke(this, new ListenerEventArgs() { Channel = channel, EventId = eventId });
            }
            catch (Exception ex)
            {
                _logger.LogError(
                    $"Disconnection from Listener failed: Stack Trace: {ex.StackTrace} Message: {ex.Message}", LogSourceType.LogEntry);
            }
        }       
    }
}
