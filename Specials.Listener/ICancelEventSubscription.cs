﻿using System.Threading;

namespace Specials.Listener
{
    public interface ICancelEventSubscription
    {
        event CancelEventSubscription.CancelEventHandler CancelReceived;
        void ValidateEventSubscriptionAsync(string channel, long eventId);
        
    }
}