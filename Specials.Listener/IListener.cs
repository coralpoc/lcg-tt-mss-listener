﻿namespace Specials.Listener
{
    public interface IListener
    {
        void ListenTo(string channel, long eventId);
    }
}