﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Specials.Domain.Models.Listener;
using Specials.Listener.Helper;
using Specials.Listener.Http;
using Specials.Listener.Storage;
using Specials.Kafka;
using System;
using System.Diagnostics;
using System.Threading;
using Specials.Listener.Utilities;
using StackExchange.Redis;
using System.Net.Sockets;

namespace Specials.Listener
{
    public class Listener : IListener
    {
        private readonly ILogger<Listener> _logger;
        private readonly IListenerHttpClient _listener;
        private readonly ICancelEventSubscription _cancelEventSubscription;
        private readonly IStorageSubscription _storage;
        private readonly IKafkaProducer _kafkaProducer;
        private readonly IListenerHelper _listenerHelper;
        
        private CancellationTokenSource _cleanupCancellationToken;
        private CancellationTokenSource _token = new CancellationTokenSource();

        public Listener(ILogger<Listener> logger, IListenerHttpClient listener,
            ICancelEventSubscription cancelEventSubscription, IStorageSubscription storage, IKafkaProducer kafkaProducer, IListenerHelper listenerHelper)
        {
            _logger = logger;
            _listener = listener;
            _cancelEventSubscription = cancelEventSubscription;
            _storage = storage;
            _cancelEventSubscription.CancelReceived += CancelEventSubscription_CancelReceived;
            _listener.MessageReceived += Listener_MessageReceived;
            _listener.Disconnect += Listener_Disconnect;
            _listener.RefreshSubscription += Listener_Refresh;
            _kafkaProducer = kafkaProducer;
            _listenerHelper = listenerHelper;
        }

        public void ListenTo(string channel, long eventId)
        {
            try
            {
                if (!_storage.IsSubscriptionExists(channel, eventId))
                {
                    _logger.LogInformation($"Stopped listening thread for ch:{channel}:e:{eventId} on refresh as the subscription no longer exists.", LogSourceType.LogEntry);
                    CancelEventSubscription_CancelReceived(this, true);                   
                    _logger.LogInformation($"Cancelled Event subscription for ch:{channel}:e:{eventId}", LogSourceType.LogEntry);
                    return;
                }
                StartListener(channel, eventId);
            }
            catch (Exception e)
            {
                _logger.LogError($"Unknown error thrown from ListenTo {e.Message} {e.StackTrace}", LogSourceType.LogEntry);
            }
        }

        private void StartListener(string channel, long eventId)
        {
            try
            {
                _listener.ListenAsync(channel, eventId, _token.Token);
            }
            catch (Exception e)
            {
                _logger.LogError($"Error thrown from ListenAsync {e.Message} {e.StackTrace}", LogSourceType.LogEntry);
            }
        }


        private void CancelEventSubscription_CancelReceived(object sender, bool args)
        {
            try
            {
                _token.Cancel(false);
            }           
            catch (Exception e)
            {
                _logger.LogError($"Error thrown from CancelEventSubscription_CancelReceived {e.Message} {e.StackTrace}", LogSourceType.LogEntry);
            }
        }

        private void Listener_Disconnect(object sender, ListenerEventArgs args)
        {
            //try
            //{
            //    _storage.CancelSubscribedEventOnContainer(args.Channel, args.EventId);
            //}
            //catch (Exception e)
            //{
            //    _logger.LogError($"Unknown error thrown from Listener_Disconnect {e.Message} {e.StackTrace}");
            //}
        }

        private void Listener_MessageReceived(object sender, KafkaMessageEventArgs args)
        {
            try
            {
                
                _logger.LogInformation($"Listener message received {args}", LogSourceType.LogEntry);
                
                 var  keyExist = _listenerHelper.FilterListenerUpdate(args.Channel, args.EventId, args.Message);

                if (keyExist)
                {
                    _cleanupCancellationToken?.Cancel(false);
                    if (!_storage.IsSubscriptionExists(args.Channel, args.EventId))
                    {
                        _logger.LogInformation($"Stopped listening thread for ch:{args.Channel}:e:{args.EventId} on refresh as the subscription no longer exists.", LogSourceType.LogEntry);
                        CancelEventSubscription_CancelReceived(this, true);

                        _logger.LogInformation($"Cancelled Event subscription for ch:{args.Channel}:e:{args.EventId}", LogSourceType.LogEntry);
                        return;
                    }

                    _kafkaProducer.SendMessageToKafka(JsonConvert.SerializeObject(args), args.EventId);
                }
            }           
            catch (Exception e)
            {
                _logger.LogError($"Unknown error thrown from Listener message received : {e.Message} {e.StackTrace}", LogSourceType.LogEntry);
            }
        }

        private void Listener_Refresh(object sender, string channel, long eventId, Guid subscriptionId)
        {
            try
            {
                _token.Token.ThrowIfCancellationRequested();
                var timer = new Stopwatch();
                _token.Cancel(false);
                timer.Start();
                _token = new CancellationTokenSource();

                if (!_storage.IsSubscriptionExists(channel, eventId))
                {
                    _logger.LogInformation($"Stopped listening thread for ch:{channel}:e:{eventId}\nGUID: {subscriptionId} on refresh as the subscription no longer exists.", LogSourceType.LogEntry);
                    return;
                }

                StartListener(channel, eventId);
                _logger.LogInformation($"ch:{channel}:e:{eventId} Elapsed time for refreshed listener:{timer.Elapsed}\nGUID: {subscriptionId}", LogSourceType.LogEntry);
            }
            catch (OperationCanceledException oce)
            {
                _logger.LogError($"Opeartion Cancelled on refresh subscription for ch:{channel}:e:{eventId}\nGUID: {subscriptionId} . Error: {oce.Message} StackTrace: {oce.StackTrace}");
                _logger.LogInformation($"Stopped REFRESH thread for ch:{channel}:e:{eventId}", LogSourceType.LogEntry);
            }           
            catch (Exception ex)
            {
                _logger.LogError(
                    $"EventId: {eventId} \nUnable to refresh subscription {ex.StackTrace} Message: {ex.Message}\nGUID: {subscriptionId}", LogSourceType.LogEntry);
                _logger.LogInformation(
                    $"CANCELLED REFRESH SUBSCRIPTION for ch:{channel}:e:{eventId}\nGUID: {subscriptionId}", LogSourceType.LogEntry);
                 Listener_Refresh(this, channel, eventId, subscriptionId);
            }
        }

    }
}