﻿using System.Collections.Generic;
using Specials.Domain.Models.DTO;

namespace Specials.Listener.Storage
{
    public interface IStorageSubscription
    {
        IList<SubscriptionDto> GetSubscribedEvents();

        SubscriptionDto GetSubscribedEvent(string channel, long eventId);

        IList<SubscriptionDto> GetUnsubscribedEvents();

        public void CancelSubscribedEventOnContainer(string channel, long eventId);

        public void CancelSubscribedEventsNotStartedOnContainer(int numberOfEventsToCancel);

        void CancelSubscribedEventsOnContainer();
        
        IEnumerable<SubscriptionDto> GetSubscribedEventsOnContainer();

        public void PostPodName(SubscriptionDto subscription);

        void PostPodNameToEvent(string channel, long eventId);

        public void CleanupSubscription(string channel, long eventId);
        bool IsSubscriptionExists(string channel, long eventId);

    }
}