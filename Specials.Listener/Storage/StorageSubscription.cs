﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Options;
using Specials.Domain.Models.Domain;
using Specials.Domain.Models.DTO;
using Specials.RedisStorage;

namespace Specials.Listener.Storage
{
    public class StorageSubscription : IStorageSubscription
    {
        private readonly Config _config;
        private readonly IRedisStorage _storage;

        public StorageSubscription(IRedisStorage storage, IOptions<Config> config)
        {
            _storage = storage;
            _config = config.Value;
        }

        public IList<SubscriptionDto> GetSubscribedEvents()
        {
            var keys = _storage.GetKeys("subs");
            var subscription =  _storage.GetObjects<SubscriptionDto>(keys).ToList();
              

            return subscription;
        }

        public IEnumerable<SubscriptionDto> GetSubscribedEventsNotStartedOnContainer()
        {
            var keys = _storage.GetKeys("special");

            var specialsStarted =  _storage.GetObjects<SpecialDto>(keys).Where(a => a.EventIsStarted).ToList() ;

            var subscribedEventsOnContainer = GetSubscribedEventsOnContainer().ToList();

            var subscribedEventsNotStarted = subscribedEventsOnContainer;

            foreach (var subscribedEventOnContainer in subscribedEventsOnContainer)
            {
                foreach (var specialDto in specialsStarted)
                {
                    if(subscribedEventOnContainer.SpecialKeyIds.Contains(specialDto.Id.ToString()))
                    {
                        subscribedEventsNotStarted.Remove(subscribedEventOnContainer);
                        break;
                    };
                }
            }

            return subscribedEventsNotStarted;
        }

        public SubscriptionDto GetSubscribedEvent(string channel, long eventId)
        {
            var subscribedEvents = GetSubscribedEvents();

            if (!subscribedEvents.Any())
            {
                return null;
            }

            var subscription = subscribedEvents.FirstOrDefault(a => a.Channel == channel && a.EventId == eventId);
            return subscription;
        }


        public IList<SubscriptionDto> GetUnsubscribedEvents()
        {
            var subscribedEvents = GetSubscribedEvents();

            if (!subscribedEvents.Any())
            {
                return new List<SubscriptionDto>();
            }

            var subscriptionsWithBlankPodNames = subscribedEvents.Where(a => a.PodName == string.Empty).ToList();
            return subscriptionsWithBlankPodNames;
        }

        public void PostPodNameToEvent(string channel, long eventId)
        {
            var subscription = _storage.GetObject<SubscriptionDto>($"subs:ch:{channel}:e:{eventId}");

            if (subscription == null) return;

            PostPodName(subscription);
        }


        public void CancelSubscribedEventOnContainer(string channel, long eventId)
        {
            var subscribedEvent = GetSubscribedEvent(channel, eventId);

            subscribedEvent.PodName = string.Empty;
            subscribedEvent.Subscribed = DateTime.Now;
            _storage.SetObject(subscribedEvent.KeyId, subscribedEvent);
        }


        public void CancelSubscribedEventsOnContainer()
        {
            var subscribedEvents = GetSubscribedEventsOnContainer();

            foreach (var subscribedEvent in subscribedEvents)
            {
                subscribedEvent.PodName = string.Empty;
                subscribedEvent.Subscribed = DateTime.MinValue;
                _storage.SetObject(subscribedEvent.KeyId, subscribedEvent);
            }
        }

        public void CancelSubscribedEventsNotStartedOnContainer(int numberOfEventsToCancel)
        {
            var subscribedEvents = GetSubscribedEventsNotStartedOnContainer();

            foreach (var subscribedEvent in subscribedEvents.Take(numberOfEventsToCancel))
            {
                subscribedEvent.PodName = string.Empty;
                subscribedEvent.Subscribed = DateTime.MinValue;
                _storage.SetObject(subscribedEvent.KeyId, subscribedEvent);
            }
        }


        public IEnumerable<SubscriptionDto> GetSubscribedEventsOnContainer()
        {
            var subscriptionsWithBlankPodNames =
                GetSubscribedEvents().Where(a => a.PodName == _config.ContainerName).ToList();
            return subscriptionsWithBlankPodNames;
        }

        public void PostPodName(SubscriptionDto subscription)
        {
            subscription.PodName = _config.ContainerName;
            subscription.Subscribed = DateTime.Now;
            _storage.SetObject(subscription.KeyId, subscription);
        }

        public void CleanupSubscription(string channel, long eventId)
        {
            var subscription = _storage.GetObject<SubscriptionDto>($"subs:ch:{channel}:e:{eventId}");
            subscription.PodName = "";
            subscription.Subscribed = DateTime.MinValue;
            _storage.SetObject(subscription.KeyId, subscription);
        }

        public bool IsSubscriptionExists(string channel, long eventId)
        {
            var subscription = _storage.GetObject<SubscriptionDto>($"subs:ch:{channel}:e:{eventId}");
            if (subscription != null)                 
                return true;
            else
            {
                return false;
            }
        }
    }
}