﻿using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;

namespace Specials.Listener.Utilities
{
    public static class CustomLoggerExtension
    {
        /// <summary>
        ///     Logs the debug.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="message">The message.</param>
        /// <param name="logSourceType">Type of the monitored item.</param>
        public static void LogDebug(this ILogger logger, string message, LogSourceType logSourceType)
        {
            var logEvent = new LogEvent(message)
                .AddProperty("eventType", logSourceType);

            logger.Log(LogLevel.Debug, default(EventId), logEvent, null, LogEvent.Formatter);
        }

        /// <summary>
        ///     Logs the critical.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="message">The message.</param>
        /// <param name="logSourceType">Type of the monitored item.</param>
        public static void LogCritical(this ILogger logger, string message, LogSourceType logSourceType)
        {
            var logEvent = new LogEvent(message)
                .AddProperty("eventType", logSourceType);

            logger.Log(LogLevel.Critical, default(EventId), logEvent, null, LogEvent.Formatter);
        }

        /// <summary>
        ///     Logs the error.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="message">The message.</param>
        /// <param name="logSourceType">Type of the monitored item.</param>
        /// <param name="exception">The exception.</param>
        public static void LogError(this ILogger logger, string message, LogSourceType logSourceType,
            Exception exception)
        {
            var logEvent = new LogEvent(message)
                .AddProperty("eventType", logSourceType);

            logger.Log(LogLevel.Error, default(EventId), logEvent, exception, LogEvent.Formatter);
        }


        /// <summary>
        ///     Logs the error.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="message">The message.</param>
        /// <param name="logSourceType">Type of the monitored item.</param>
        public static void LogError(this ILogger logger, string message, LogSourceType logSourceType)
        {
            var logEvent = new LogEvent(message)
                .AddProperty("eventType", logSourceType);

            logger.Log(LogLevel.Error, default(EventId), logEvent, null, LogEvent.Formatter);
        }

        /// <summary>
        ///     Logs the information.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="message">The message.</param>
        /// <param name="logSourceType">Type of the monitored item.</param>
        public static void LogInformation(this ILogger logger, string message, LogSourceType logSourceType)
        {
            var logEvent = new LogEvent(message)
                .AddProperty("eventType", logSourceType);

            logger.Log(LogLevel.Information, default(EventId), logEvent, null, LogEvent.Formatter);
        }

        /// <summary>
        ///     Logs the trace.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="message">The message.</param>
        /// <param name="logSourceType">Type of the monitored item.</param>
        public static void LogTrace(this ILogger logger, string message, LogSourceType logSourceType)
        {
            var logEvent = new LogEvent(message)
                .AddProperty("eventType", logSourceType);

            logger.Log(LogLevel.Trace, default(EventId), logEvent, null, LogEvent.Formatter);
        }


        /// <summary>
        ///     Logs the warning.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="message">The message.</param>
        /// <param name="logSourceType">The log source type</param>
        public static void LogWarning(this ILogger logger, string message, LogSourceType logSourceType)
        {
            var logEvent = new LogEvent(message)
                .AddProperty("eventType", logSourceType);

            logger.Log(LogLevel.Warning, default(EventId), logEvent, null, LogEvent.Formatter);
        }

        /// <summary>
        ///     Logs the metric.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="message">The message.</param>
        /// <param name="metricType">Type of the metric.</param>
        /// <param name="metricValue">The metric value.</param>
        /// <param name="correlationId">The correlation identifier.</param>
        /// <param name="transactionId">The transaction identifier</param>
        public static void LogMetric(this ILogger logger, string message, string metricType, double metricValue)
        {
            var logEvent = new LogEvent(message)
                .AddProperty("eventType", LogSourceType.Metric)
                .AddProperty("metricType", metricType)
                .AddProperty("metricValue", metricValue);

            logger.Log(LogLevel.Information, default(EventId), logEvent, null, LogEvent.Formatter);
        }
    }

    internal class LogEvent : IEnumerable<KeyValuePair<string, object>>
    {
        private readonly List<KeyValuePair<string, object>> _properties = new List<KeyValuePair<string, object>>();

        public LogEvent(string message)
        {
            Message = message;
        }

        public string Message { get; }

        public static Func<LogEvent, Exception, string> Formatter { get; } = (l, e) => l.Message;

        public IEnumerator<KeyValuePair<string, object>> GetEnumerator()
        {
            return _properties.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public LogEvent AddProperty(string name, object value)
        {
            _properties.Add(new KeyValuePair<string, object>(name, value));
            return this;
        }

    }

    public enum LogSourceType
    {
        Metric,
        LogEntry
    }
}