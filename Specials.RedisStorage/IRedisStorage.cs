﻿using System.Collections.Generic;

namespace Specials.RedisStorage
{
    public interface IRedisStorage
    {
        IEnumerable<string> GetKeys(string type);
        T GetObject<T>(string key);
        IEnumerable<T> GetObjects<T>(IEnumerable<string> keys);
        void SetObject<T>(string key, T objectItem);
        void Remove(string key);

        bool IsKeyExists(string key);
    }
}