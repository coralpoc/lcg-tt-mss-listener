﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace Specials.RedisStorage
{
    public class RedisStorage : IRedisStorage
    {
        private readonly IDatabase _redisDb;
        private readonly IServer _server;
        private readonly int _expires;        

        public RedisStorage(string connectionString, int expires, int redis_db_name,int keep_alive,int connect_timeout,int sync_timeout)
        {
            var options = ConfigurationOptions.Parse(connectionString);
            options.KeepAlive = keep_alive;
            options.ConnectTimeout = connect_timeout;
            options.SyncTimeout = sync_timeout;
            options.AbortOnConnectFail = false;
            options.ReconnectRetryPolicy = new LinearRetry(5000);
            options.ConnectRetry = 5;
            var redis = new Lazy<ConnectionMultiplexer>(() => ConnectionMultiplexer.Connect(options));           
            _redisDb = redis.Value.GetDatabase(db: redis_db_name);
            var endpoints = _redisDb.Multiplexer.GetEndPoints();
            _server = _redisDb.Multiplexer.GetServer(endpoints.First());
            _expires = expires;
        }

        public byte[] Get(string key)
        {
            return _redisDb.StringGet(key, CommandFlags.PreferReplica);
        }

        public bool IsKeyExists(string key)
        {
            return _redisDb.KeyExists(key);
        }

        public string GetString(string key)
        {
            var bytes = Get(key);
            if (bytes == null)
                return "";

            var result = Encoding.ASCII.GetString(bytes);
            return result;
        }

        public T GetObject<T>(string key)
        {
            var json = GetString(key);
            var result = JsonConvert.DeserializeObject<T>(json);
            return result;
        }

        public IEnumerable<T> GetObjects<T>(IEnumerable<string> keys)
        {
            return keys.Select(GetObject<T>).ToList();
        }

        public List<string> GetAllKeys(string type)
        {
            var keys = _server.Keys(_redisDb.Database, pattern: $"{type}*");
            var allKeys = keys.Select(redisKey => redisKey.ToString()).ToList();
            return allKeys;
        }

        public IEnumerable<string> GetKeys(string type)
        {
            var allKeys = GetAllKeys(type);

            //var keys = _server.Keys(_redisDb.Database, pattern: $"{type}*").Select(redisKey => redisKey.ToString());
            return allKeys;
        }

        public Task<byte[]> GetAsync(string key, CancellationToken token = default)
        {
            return Task.Run(() => Encoding.ASCII.GetBytes(_redisDb.StringGet(key)), token);
        }

        public void Refresh(string key)
        {
            throw new NotImplementedException();
        }

        public Task RefreshAsync(string key, CancellationToken token = default)
        {
            throw new NotImplementedException();
        }

        public void Remove(string key)
        {
            _redisDb.KeyDelete(key);
        }

        public Task RemoveAsync(string key, CancellationToken token = default)
        {
            return Task.Run(() => _redisDb.KeyDelete(key), token);
        }

        public void Set(string key, byte[] value)
        {
            _redisDb.StringSet(key, value, TimeSpan.FromSeconds(_expires));
        }

        public void SetJson(string key, string json)
        {
            var encoded = Encoding.ASCII.GetBytes(json);
            _redisDb.StringSet(key, encoded, TimeSpan.FromSeconds(_expires));
        }

        public void SetObject<T>(string key, T objectItem)
        {
            var json = JsonConvert.SerializeObject(objectItem);
            SetJson(key, json);
        }

        public Task SetAsync(string key, byte[] value, CancellationToken token = default)
        {
            return Task.Run(() => _redisDb.StringSet(key, value, TimeSpan.FromSeconds(_expires)), token);
        }
    }
}